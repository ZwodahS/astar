import astar.Graph;


class Main
{
    static inline var SAMPLES = 10000;


    public static function main()
    {
        var cacheCost: Float = 0;
        var cacheBenefit: Float = 0;

        for (i in 0...SAMPLES)
        {
            var result = run();

            cacheCost += result.cacheCost;
            cacheBenefit += result.cacheBenefit;
        }

        cacheCost = Math.round(cacheCost / SAMPLES);
        cacheBenefit = Math.round(cacheBenefit / SAMPLES);

        Sys.println('Cost to construct cache: $cacheCost%');
        Sys.println('Benefit with cache: $cacheBenefit%');
    }


    static function run(): Dynamic
    {
        var graph: Graph = new Graph(10, 10, FourWay);

        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];

        var costs = [
            0 => Graph.DefaultCosts
        ];

        graph.setWorld(world);
        graph.setCosts(costs);
        graph.solve(1, 0, 9, 9); // fill memory pools

        var timestamp: Timestamp = Timestamp.Now;

        graph.solve(1, 0, 9, 9);

        var solveWithoutCache: Float = timestamp.getElapsedMsReset();

        graph.configureCache(true, 128);

        timestamp.reset();

        graph.solve(1, 0, 9, 9);

        var solveToFillCache: Float = timestamp.getElapsedMsReset();

        graph.solve(0, 0, 9, 9);

        var solveWithCache: Float = timestamp.getElapsedMsReset();

        graph.solve(0, 0, 9, 9);

        var solveWithCache2: Float = timestamp.getElapsedMsReset();

        return {
            cacheCost: 100 * (solveToFillCache - solveWithoutCache) / solveWithoutCache,
            cacheBenefit: 100 * (solveWithoutCache - solveWithCache) / solveWithoutCache
        };
    }
}
