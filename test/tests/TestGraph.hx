package tests;

import astar.cache.PathCache;
import astar.cost.EuclideanDistance;
import Math.sqrt;
import astar.types.PathPoint;
import astar.types.Result;
import astar.SearchResult;
import astar.types.Direction;
import utest.Assert;
import astar.Graph;
import utest.ITest;


@:access(astar.Graph)
class TestGraph implements ITest
{
    var graph: Graph;

    public function new() { }


    function setup()
    {
        // Simple 10x10 grid
        graph = new Graph(10, 10, FourWay);
        graph.setCosts(defaultCosts());
    }


    function testInvalidDirection()
    {
        graph.movementDirection = cast -1;

        Assert.raises(() -> graph.getHeuristicDistance(2, 3, 2, 2), String);
    }


    function testManhattanDistance()
    {
        graph.movementDirection = FourWay;

        Assert.floatEquals(1, graph.getHeuristicDistance(2, 3, 2, 2));
        Assert.floatEquals(0, graph.getHeuristicDistance(2, 3, 2, 3));
        Assert.floatEquals(1, graph.getHeuristicDistance(2, 4, 2, 3));
        Assert.floatEquals(2, graph.getHeuristicDistance(3, 4, 2, 3));
        Assert.floatEquals(2, graph.getHeuristicDistance(1, 4, 2, 3));
        Assert.floatEquals(2, graph.getHeuristicDistance(1, 4, 2, 5));

        graph.setHeuristicWeight(2.0);
        Assert.floatEquals(4, graph.getHeuristicDistance(1, 4, 2, 5));
    }


    function testDiagonalDistance()
    {
        graph.movementDirection = EightWay;

        Assert.floatEquals(1, graph.getHeuristicDistance(2, 3, 2, 2));
        Assert.floatEquals(0, graph.getHeuristicDistance(2, 3, 2, 3));
        Assert.floatEquals(1, graph.getHeuristicDistance(2, 4, 2, 3));
        Assert.floatEquals(sqrt(2), graph.getHeuristicDistance(3, 4, 2, 3), 1e-2);
        Assert.floatEquals(sqrt(2), graph.getHeuristicDistance(1, 4, 2, 3), 1e-2);
        Assert.floatEquals(sqrt(2), graph.getHeuristicDistance(1, 4, 2, 5), 1e-2);
        Assert.floatEquals(sqrt(8), graph.getHeuristicDistance(1, 1, 3, 3), 1e-2);
        Assert.floatEquals(5.82, graph.getHeuristicDistance(1, 1, 3, 6), 1e-2);
    }


    function testEuclideanDistance()
    {
        graph.movementDirection = EightWay;
        graph.setHeuristicFunction(new EuclideanDistance());

        Assert.floatEquals(1, graph.getHeuristicDistance(2, 3, 2, 2));
        Assert.floatEquals(0, graph.getHeuristicDistance(2, 3, 2, 3));
        Assert.floatEquals(1, graph.getHeuristicDistance(2, 4, 2, 3));
        Assert.floatEquals(sqrt(2), graph.getHeuristicDistance(3, 4, 2, 3));
        Assert.floatEquals(sqrt(2), graph.getHeuristicDistance(1, 4, 2, 3));
        Assert.floatEquals(sqrt(2), graph.getHeuristicDistance(1, 4, 2, 5));
    }


    function testGetMinCost()
    {
        graph.setCosts([0 => [ N => 3, S => 2, W => 1, E => 4 ]]);

        Assert.floatEquals(1, graph.getMinCost(0xFFFF));
    }


    function testGetCellCost()
    {
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];

        graph.setWorld(world);
        graph.setCosts([0 => [ N => 3, S => 2, W => 1 ]]);

        Assert.floatEquals(3, graph.getCellCost(0, 0, N));
        Assert.floatEquals(-1, graph.getCellCost(0, 0, E));
        Assert.floatEquals(2, graph.getCellCost(0, 2, S));
    }


    function testSetInvalidWorld()
    {
        Assert.raises(() -> graph.setWorld([1, 2, 3]), String);
    }


    function testCanMoveFromCellInDirection()
    {
        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];

        var costs = defaultCosts();
        costs.set(2, [ N => 1, S => 1 ]);

        graph.setWorld(world);
        graph.setCosts(costs);

        graph.movementDirection = FourWay;
        Assert.isTrue( graph.canMoveFromCellInDirection(5, 3, E) );
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, N) );
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, W) );
        Assert.isTrue( graph.canMoveFromCellInDirection(5, 3, S) );

        graph.movementDirection = EightWay;
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, NE) );
        Assert.isTrue( graph.canMoveFromCellInDirection(5, 3, NW) );

        graph.movementDirection = EightWayHalfObstructed;
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, NE) );
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, NW) );
        Assert.isTrue( graph.canMoveFromCellInDirection(5, 3, SE) );
        Assert.isTrue( graph.canMoveFromCellInDirection(4, 4, NE) );

        graph.movementDirection = EightWayObstructed;
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, NE) );
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, NW) );
        Assert.isFalse( graph.canMoveFromCellInDirection(5, 3, SW) );

    }


    function testSimplePaths()
    {
        doPathingTest();
    }


    function testSimplePathsWithCache()
    {
        graph.configureCache(true, 128);

        doPathingTest();

        Assert.equals(2, graph.pathCache.hits);
        Assert.isTrue(graph.pathCache.misses > 0);
    }


    function doPathingTest()
    {
        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];

        var costs = defaultCosts();
        costs.set(2, [ N => 1, S => 1]);

        graph.setWorld(world);
        graph.setCosts(costs);

        // ================= STRAIGHT LINE =================
        var straightLine: SearchResult = graph.solve(1, 2, 8, 2);
        Assert.equals(Result.Solved, straightLine.result);
        Assert.equals(8, straightLine.steps);
        Assert.equals(7, straightLine.cost);
        printRoute(straightLine.path);


        // ================= STRAIGHT LINE 2 ===============
        var straightLine2: SearchResult = graph.solve(2, 2, 8, 2);
        Assert.equals(Result.Solved, straightLine2.result);
        Assert.equals(7, straightLine2.steps);
        Assert.equals(6, straightLine2.cost);
        printRoute(straightLine2.path);


        // ================= OVER WALL =====================
        var overWall: SearchResult = graph.solve(1, 9, 3, 9);
        Assert.equals(Result.Solved, overWall.result);
        Assert.equals(9, overWall.steps);
        Assert.equals(8, overWall.cost);
        printRoute(overWall.path);


        // ================= COMPLICATED ===================
        var complicated: SearchResult = graph.solve(0, 0, 9, 9);
        Assert.equals(Result.Solved, complicated.result);
        Assert.equals(31, complicated.steps);
        Assert.equals(30, complicated.cost);
        printRoute(complicated.path);


        // ================= MOVE THROUGH 2 =================
        var moveThrough2: SearchResult = graph.solve(3, 4, 3, 6);
        Assert.equals(Result.Solved, moveThrough2.result);
        Assert.equals(3, moveThrough2.steps);
        Assert.equals(2, moveThrough2.cost);
        printRoute(moveThrough2.path);


        // ================= AVOID 2 =================
        var avoid2: SearchResult = graph.solve(2, 5, 4, 5);
        Assert.equals(Result.Solved, avoid2.result);
        Assert.equals(5, avoid2.steps);
        Assert.equals(4, avoid2.cost);
        printRoute(avoid2.path);


        // ============= AVOID 2 REVERSE ============
        var avoid2rev: SearchResult = graph.solve(4, 5, 2, 5);
        Assert.equals(Result.Solved, avoid2rev.result);
        Assert.equals(5, avoid2rev.steps);
        Assert.equals(4, avoid2rev.cost);
        printRoute(avoid2rev.path);


        // ================= BLOCKED =======================
        var blocked: SearchResult = graph.solve(5, 5, 9, 0);
        Assert.equals(Result.NoSolution, blocked.result);


        // ================ BLOCKED 2 ======================
        // to cover cache hit on NoSolution
        var blocked2: SearchResult = graph.solve(5, 5, 9, 0);
        Assert.equals(Result.NoSolution, blocked2.result);


        // =============== START END SAME ==================
        var startEndSame: SearchResult = graph.solve(5, 5, 5, 5);
        Assert.equals(Result.StartEndSame, startEndSame.result);


        // ============ START OUT OF BOUNDS ================
        var startOutOfBounds: SearchResult = graph.solve(5, 10, 5, 5);
        Assert.equals(Result.OutOfBounds, startOutOfBounds.result);


        // ============ END OUT OF BOUNDS ================
        var endOutOfBounds: SearchResult = graph.solve(5, 5, 12, 5);
        Assert.equals(Result.OutOfBounds, endOutOfBounds.result);


        // ============== START IMPASSABLE =================
        var startImp: SearchResult = graph.solve(0, 1, 5, 5);
        Assert.equals(Result.StartOrEndImpassable, startImp.result);


        // ============== START IMPASSABLE =================
        var endImp: SearchResult = graph.solve(0, 0, 8, 0);
        Assert.equals(Result.StartOrEndImpassable, endImp.result);


        // =============== COMPLICATED 8-way ===============
        graph.resetCache();
        graph.movementDirection = EightWay;
        var complicated8: SearchResult = graph.solve(0, 0, 9, 9);
        Assert.equals(Result.Solved, complicated8.result);
        Assert.equals(24, complicated8.steps);
        Assert.floatEquals(25.89, complicated8.cost, 1e-2);
        printRoute(complicated8.path);
    }


    function testSolveCached()
    {
        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];

        var costs = defaultCosts();
        costs.set(2, [ N => 1, S => 1]);

        graph.setWorld(world);
        graph.setCosts(costs);

        graph.configureCache(true, 512);
        graph.movementDirection = EightWay;
        graph.resetCache();


        // =============== COMPLICATED 8-way ===============
        var complicated8: SearchResult = graph.solve(0, 0, 9, 9);
        Assert.equals(Result.Solved, complicated8.result);
        Assert.equals(24, complicated8.steps);
        Assert.floatEquals(25.89, complicated8.cost, 1e-2);

        Assert.equals(0, graph.pathCache.hits);

        var complicated8cached: SearchResult = graph.solve(0, 0, 9, 9);

        Assert.equals(1, graph.pathCache.hits);

        Assert.equals(complicated8.path.length, complicated8cached.path.length);
        Assert.equals(complicated8.steps, complicated8cached.steps);
        Assert.floatEquals(complicated8.cost, complicated8cached.cost);

        for (i in 0...complicated8.path.length)
        {
            Assert.equals(complicated8.path[i].x, complicated8cached.path[i].x);
            Assert.equals(complicated8.path[i].y, complicated8cached.path[i].y);
            Assert.floatEquals(complicated8.path[i].costFromStart, complicated8cached.path[i].costFromStart);
        }
    }


    function testIssueNo3()
    {
        graph = new Graph(32, 32, EightWayObstructed);
        graph.setCosts(defaultCosts());

        var world: Array<Int> = [for (_ in 0...32*32) 0];
        graph.setWorld(world);

        var set = (x: Int) ->
        {
            world[x] = 1;
            var res = graph.solve(16, 16, 0, 0);
            Assert.equals(Result.Solved, res.result);
        };
        set(263);
        set(231);
        set(199);
        set(167);
        set(135);
        set(103);
        set(71);
        set(39);
        set(40);
        set(41);
        set(42);
        set(43);
        set(44);
        set(45);
        set(46);
        set(47);
        set(48);
        set(49);
        set(50);
        set(51);
        set(52);
        set(53);
        set(54);
        set(55);
        set(56);
        set(57);
        set(58);
        set(295);
        set(296);
        set(298);
        set(299);
        set(297);
    }


    function testConfigureCache()
    {
        Assert.isNull(graph.pathCache);
        Assert.isFalse(graph.cacheEnabled);

        Assert.raises(() -> graph.configureCache(true), String);

        graph.configureCache(true, 512);

        Assert.notNull(graph.pathCache);
        Assert.isTrue(graph.cacheEnabled);
        Assert.equals(512, graph.pathCache.size);

        var curCache: PathCache = graph.pathCache;

        graph.configureCache(false);

        Assert.notNull(graph.pathCache);
        Assert.isFalse(graph.cacheEnabled);
        Assert.equals(curCache, graph.pathCache);

        graph.configureCache(true);

        Assert.notNull(graph.pathCache);
        Assert.isTrue(graph.cacheEnabled);
        Assert.equals(curCache, graph.pathCache);

        graph.configureCache(false);

        Assert.notNull(graph.pathCache);
        Assert.isFalse(graph.cacheEnabled);
        Assert.equals(curCache, graph.pathCache);

        graph.configureCache(true, 256);

        Assert.notNull(graph.pathCache);
        Assert.isTrue(graph.cacheEnabled);
        Assert.equals(curCache, graph.pathCache);
        Assert.equals(256, graph.pathCache.size);
    }


    static function defaultCosts(): Map<Int, Map<Direction, Float>>
    {
        return [
            0 => Graph.DefaultCosts
        ];
    }


    function printRoute(path: Array<PathPoint>)
    {
        var buf = new StringBuf();
        buf.add('\n');

        var inPath = (x: Int, y: Int) ->
        {
            for (p in path)
            {
                if (p.x == x && p.y == y)
                    return true;
            }

            return false;
        };

        for (y in 0...graph.height)
        {
            for (x in 0...graph.width)
            {
                var val = graph.getCellValue(x, y);

                if (inPath(x, y))
                {
                    buf.add('x ');
                }
                else
                {
                    buf.add('$val ');
                }

            }

            buf.add('\n');
        }

        trace(buf.toString());
    }
}
