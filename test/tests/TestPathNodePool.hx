package tests;

import astar.types.PathNode;
import astar.types.PathNodePool;
import utest.ITest;
import utest.Assert;


class TestPathNodePool implements ITest
{
    var pool: PathNodePool;


    public function new() { }


    function setup()
    {
        pool = new PathNodePool();
    }


    function testPooling()
    {
        var node: PathNode = pool.get();

        pool.put(node);

        Assert.equals(node, pool.get());
    }


    function testFill()
    {
        pool.fill(20);

        Assert.equals(20, pool.getSize());

        for (i in 0...20)
        {
            Assert.notNull(pool.get());

            Assert.equals(19 - i, pool.getSize());
        }
    }


    function testClear()
    {
        pool.fill(20);

        Assert.equals(20, pool.getSize());

        pool.clear();

        Assert.equals(0, pool.getSize());

        Assert.notNull(pool.get());

        Assert.equals(0, pool.getSize());
    }
}
