package tests;

import astar.types.PathNodePool;
import utest.Assert;
import astar.wrappers.ClosedSet;
import utest.ITest;


class TestClosedSet implements ITest
{
    var set: ClosedSet;


    public function new() { }


    function setup()
    {
        set = new ClosedSet(100, 100);
    }


    function test()
    {
        Assert.isFalse( set.contains(12, 13) );

        var n = new PathNodePool().getNode(12, 13, 0, 0);

        set.addNode(n);

        Assert.isTrue( set.contains(12, 13) );
    }
}
