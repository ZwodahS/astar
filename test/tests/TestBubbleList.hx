package tests;

import seedyrng.Seedy;
import utest.Assert;
import utest.ITest;
import astar.cache.BubbleList;


class Item
{
	public var next: Item;
	public var previous: Item;
	public var value: Int;


	public inline function new(value: Int) { this.value = value; }
}


@:access(astar.cache.BubbleList)
class TestBubbleList implements ITest
{
	var list: BubbleList<Item>;


	public function new() { }


	function setup()
	{
		list = new BubbleList<Item>(5);
	}


	function testInsert()
	{
		insert(1);
		assertList([ 1 ], 1);

		insert(3);
		assertList([ 3, 1 ], 3);

		insert(2);
		assertList([ 3, 2, 1 ], 2);

		insert(4);
		assertList([ 3, 4, 2, 1 ], 4);

		insert(5);
		assertList([ 3, 4, 5, 2, 1 ], 5);

		// Now to exceed capacity.
		var ret: Item;
		ret = list.insert(new Item(9));
		Assert.equals(1, ret.value);
		assertList([ 3, 4, 9, 5, 2 ]);

		ret = list.insert(new Item(9));
		Assert.equals(2, ret.value);
		assertList([ 3, 4, 9, 9, 5 ]);

		ret = list.insert(new Item(9));
		Assert.equals(5, ret.value);
		assertList([ 3, 4, 9, 9, 9 ]);
	}


	function testBubble()
	{
		var i1 = insert(1);
		var i3 = insert(3);
		var i2 = insert(2);
		var i4 = insert(4);
		var i5 = insert(5);

		assertList([ 3, 4, 5, 2, 1 ], 5);

		list.bubble(i3);
		assertList([ 3, 4, 5, 2, 1 ], 5);

		list.bubble(i4);
		assertList([ 4, 3, 5, 2, 1 ], 5);

		list.bubble(i5);
		assertList([ 4, 5, 3, 2, 1 ], 3);

		list.bubble(i2);
		assertList([ 4, 5, 2, 3, 1 ], 2);

		list.bubble(i1);
		assertList([ 4, 5, 2, 1, 3 ], 2);

		list.bubble(i1);
		assertList([ 4, 5, 1, 2, 3 ], 1);

		list.bubble(i1);
		assertList([ 4, 1, 5, 2, 3 ], 5);

		list.bubble(i1);
		assertList([ 1, 4, 5, 2, 3 ], 5);
	}


	function testPop()
	{
		var i1 = insert(1);
		var i3 = insert(3);
		var i2 = insert(2);
		var i4 = insert(4);
		var i5 = insert(5);

		assertList([ 3, 4, 5, 2, 1 ], 5);

		Assert.equals(i1, list.pop());
		assertList([ 3, 4, 5, 2 ], 4);

		Assert.equals(i2, list.pop());
		assertList([ 3, 4, 5 ], 4);

		list.insert(i1);
		assertList([ 3, 1, 4, 5 ], 1);

		Assert.equals(i5, list.pop());
		assertList([ 3, 1, 4 ], 1);

		Assert.equals(i4, list.pop());
		assertList([ 3, 1 ], 3);

		Assert.equals(i1, list.pop());
		assertList([ 3 ], 3);

		Assert.equals(i3, list.pop());
		assertList([ ]);

		Assert.isNull( list.pop() );

		Assert.isNull(list.head);
		Assert.isNull(list.tail);
		Assert.isNull(list.middle);
	}


	function testInsertBetween()
	{
		var i1 = new Item(1);
		var i2 = new Item(2);
		var i3 = new Item(3);

		list.insertBetween(null, null, i1);
		Assert.isNull(i1.next);
		Assert.isNull(i1.previous);
	}


	function testRemove()
	{
		list = new BubbleList<Item>(6);

		var i1 = insert(1);
		var i3 = insert(3);
		var i2 = insert(2);
		var i4 = insert(4);
		var i5 = insert(5);
		var i6 = insert(6);

		assertList([ 3, 4, 6, 5, 2, 1 ], 6);

		list.remove(i5);

		assertList([ 3, 4, 6, 2, 1 ], 2);

		list.remove(i6);

		assertList([ 3, 4, 2, 1 ], 4);

		list.remove(i3);

		assertList([ 4, 2, 1 ], 2);

		list.remove(i1);

		assertList([ 4, 2 ], 4);

		list.insert(i1);

		assertList([ 4, 1, 2 ], 1);

		list.remove(i2);

		assertList([ 4, 1 ], 4);

		list.remove(i4);

		assertList([ 1 ], 1);

		list.remove(i1);

		assertList([ ]);

		Assert.raises(() -> list.remove(i1), String);
	}


	function testLargeRandom()
	{
		var listSize: Int = 256;
		var iterations: Int = 1000;

		list = new BubbleList<Item>(listSize);

        var numbers: Array<Int> = [for (i in 0...iterations) i];
		Seedy.shuffle(numbers);

		var expected: Array<Item> = [];

		for (i in 0...iterations)
		{
			var item: Item = new Item(numbers[i]);

			var popped: Item = list.insert(item);
			expected.push(item);

			if (popped != null)
			{
				expected.remove(popped);
			}

			// With a 30% chance, bubble an element.
			if (Seedy.random() < 0.3)
			{
				var toBubble: Item = Seedy.choice(expected);

				list.bubble(toBubble);
			}

			assertListOutOfOrder(expected);
		}
	}


	function insert(val: Int): Item
	{
		var i = new Item(val);
		if (list.length < list.capacity)
		{
			var r: Item = list.insert(i);
			Assert.isNull( r );
		}
		else
		{
			var r: Item = list.insert(i);
			Assert.notNull( r );
		}
		Assert.isTrue( list.length <= list.capacity );
		return i;
	}


	function assertList(expected: Array<Int>, ?middle: Int)
	{
		Assert.equals(expected.length, list.length);

		if (middle != null)
		{
			Assert.equals(middle, list.middle.value);
		}

		var values: Array<Int> = [];
		var iter: Item = list.head;
		var equal: Bool = true;

		for (i in 0...expected.length)
		{
			Assert.notNull(iter);

			equal = equal && (expected[i] == iter.value);

			values.push(iter.value);

			if (iter == iter.next)
			{
				Assert.fail("Infinite loop.");
				return;
			}

			iter = iter.next;
		}

		Assert.equals(values.length, list.length);

		if (!equal)
		{
			Assert.fail('expected $expected but it is $values');
		}
		else
		{
			Assert.pass();
		}
	}


	function assertListOutOfOrder(expected: Array<Item>)
	{
		var iter: Item = list.head;
		var expCopy: Array<Item> = expected.copy();

		while (iter != null)
		{
			var ret: Bool = expCopy.remove(iter);
			Assert.isTrue(ret);

			if (!ret)
			{
				return;
			}

			iter = iter.next;
		}

		Assert.equals(0, expCopy.length);
	}
}
