package tests;

import astar.types.Direction;
import utest.Assert;
import utest.ITest;
import astar.types.Direction.*;


class TestDirection implements ITest
{
    public function new() { }


    function testDir()
    {
        checkDir(N,  0, 1);
        checkDir(S,  0, -1);
        checkDir(W,  -1, 0);
        checkDir(E,  1, 0);
        checkDir(NW, -1, 1);
        checkDir(NE, 1, 1);
        checkDir(SW, -1, -1);
        checkDir(SE, 1, -1);
    }


    inline function checkDir(dir: Direction, x: Int, y: Int)
    {
        Assert.equals(x, dir.dirX(), '$dir $x');
        Assert.equals(y, dir.dirY(), '$dir $y');
    }
}
