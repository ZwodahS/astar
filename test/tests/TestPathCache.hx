package tests;

import astar.cache.CacheResult;
import astar.types.PathPoint;
import astar.types.Result;
import astar.SearchResult;
import astar.types.PathNode;
import astar.Graph;
import utest.Assert;
import astar.cache.PathCache;
import utest.ITest;


@:access(astar.Graph)
@:access(astar.cache.PathCache)
@:access(astar.cache.BubbleList)
class TestPathCache implements ITest
{
    var cache: PathCache;


    public function new() { }


    function setup()
    {
        cache = new PathCache(1024);
    }


    function testHash()
    {
        // Good enough is we want no collisions in a small grid.

        var size: Int = 25;

        var hashes: Map<Int, Bool> = [];

        for (x1 in 0...size)
        {
            for (y1 in 0...size)
            {
                for (x2 in 0...size)
                {
                    for (y2 in 0...size)
                    {
                        var hash: Int = PathCache.hash(x1, y1, x2, y2);

                        Assert.isFalse( hashes.exists(hash) );

                        hashes.set(hash, true);
                    }
                }
            }
        }
    }


    function testAddSolution()
    {
        var graph: Graph = loadTestGraph();

        var solution: SearchResult = graph.solve(0, 0, 9, 9);
        Assert.equals(Result.Solved, solution.result);

        var endNode: PathNode = graph.findPath(0, 0, 9, 9);
        Assert.notNull(endNode.previous);

        cache.addSolution(endNode);

        Assert.equals(solution.path.length - 1, countCachedPaths());

        for (startX in 0...10)
        {
            for (startY in 0...10)
            {
                var hash: Int = PathCache.hash(startX, startY, 9, 9);

                var existsInPath: Bool = false;

                for (node in solution.path)
                {
                    if (node.x == startX && node.y == startY)
                    {
                        existsInPath = true;
                        break;
                    }
                }

                if (existsInPath && (startX != 9 || startY != 9))
                {
                    Assert.notNull( cache.get(hash) );
                }
                else
                {
                    Assert.isNull( cache.get(hash) );
                }
            }
        }
    }


    function testSolveCached()
    {
        var graph: Graph = loadTestGraph();

        var solution: SearchResult = graph.solve(0, 0, 9, 9);
        var solutionPath: Array<PathPoint> = solution.path.copy();
        var endNode: PathNode = graph.findPath(0, 0, 9, 9);

        cache.addSolution(endNode);

        for (startX in 0...10)
        {
            for (startY in 0...10)
            {
                var existsInPath: Bool = false;

                for (node in solutionPath)
                {
                    if (node.x == startX && node.y == startY)
                    {
                        existsInPath = true;
                        break;
                    }
                }

                solution.path.splice(0, solution.path.length);
                solution.resetCost();

                var cacheResult: CacheResult = cache.solve(startX, startY, 9, 9, solution);

                if (existsInPath && (startX != 9 || startY != 9))
                {
                    Assert.equals(CacheResult.Hit, cacheResult);

                    Assert.equals(startX, solution.path[0].x);
                    Assert.equals(startY, solution.path[0].y);

                    Assert.equals(9, solution.path[solution.path.length - 1].x);
                    Assert.equals(9, solution.path[solution.path.length - 1].y);

                    Assert.equals(solution.path.length - 1, solution.cost);
                    Assert.equals(solution.path.length, solution.steps);

                    for (i in 0...solution.path.length)
                    {
                        Assert.equals(solutionPath[solutionPath.length - 1 - i].x, solution.path[solution.path.length - 1 - i].x);
                        Assert.equals(solutionPath[solutionPath.length - 1 - i].y, solution.path[solution.path.length - 1 - i].y);
                    }
                }
                else
                {
                    Assert.notEquals(CacheResult.Hit, cacheResult);
                }
            }
        }
    }


    function testReset()
    {
        var graph: Graph = loadTestGraph();

        var endNode: PathNode = graph.findPath(0, 0, 9, 9);

        cache.addSolution(endNode);

        Assert.equals(0, cache.cachedPathPool.getSize());
        Assert.equals(0, cache.cachedPathNodePool.getSize());

        cache.reset();

        Assert.equals(0, cache.cache.length);
        Assert.equals(0, countCachedPaths());
        Assert.isNull(cache.cache.head);
        Assert.isNull(cache.cache.middle);
        Assert.isNull(cache.cache.tail);

        Assert.equals(30, cache.cachedPathPool.getSize());
        Assert.equals(31, cache.cachedPathNodePool.getSize());

        // Test with freeMemory
        cache.addSolution(endNode);

        cache.reset(true);

        Assert.equals(0, cache.cache.length);
        Assert.equals(0, countCachedPaths());
        Assert.isNull(cache.cache.head);
        Assert.isNull(cache.cache.middle);
        Assert.isNull(cache.cache.tail);

        Assert.equals(0, cache.cachedPathPool.getSize());
        Assert.equals(0, cache.cachedPathNodePool.getSize());
    }


    function loadTestGraph(): Graph
    {
        var graph: Graph = new Graph(10, 10, FourWay);

        // 0: floor
        // 1: wall
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 0, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];

        graph.setWorld(world);
        graph.setCosts([ 0 => Graph.DefaultCosts ]);

        return graph;
    }


    function countCachedPaths(): Int
    {
        var count: Int = 0;

        for (path in cache.cacheHashMap)
        {
            if (path != null) count++;
        }

        return count;
    }
}
