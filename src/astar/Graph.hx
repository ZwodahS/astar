package astar;

import haxe.macro.Expr.Catch;
import astar.cache.CacheResult;
import Math.sqrt;
import astar.cache.PathCache;
import astar.cost.DiagonalDistance;
import astar.cost.ManhattanDistance;
import astar.cost.HeuristicFunction;
import astar.types.Direction;
import astar.types.PathNode;
import astar.wrappers.ClosedSet;
import astar.types.SortedPathNodeList;
import astar.types.PathNodePool;


class Graph
{
    public var width(default, null): Int;
    public var height(default, null): Int;
    public var movementDirection(default, null): MovementDirection;

    var world: Array<Int>;
    var costs: Map<Int, Map<Direction, Float>>;
    var heuristicFunction: HeuristicFunction;
    var heuristicWeight: Float;

    var nodePool: PathNodePool;
    var pathCache: PathCache;
    var cacheEnabled: Bool;

    var openList: SortedPathNodeList;
    var closedList: ClosedSet;
    var goalX: Int;
    var goalY: Int;


    public function new(width: Int, height: Int, movementDirection: MovementDirection)
    {
        this.width = width;
        this.height = height;
        this.movementDirection = movementDirection;

        heuristicWeight = 1.0;
        nodePool = new PathNodePool();
        pathCache = null;
        cacheEnabled = false;

        openList = new SortedPathNodeList();
        closedList = new ClosedSet(width, height);
    }


    /**
        Loads the world grid into the graph.

        The array should have `width`x`height` elements, which contain the grid packed
        row by row. So the first element is the cell 0x0, the second element is the cell 1x0 and so on.

        The values in the cells of the grid are arbitrary, and shall be chosen by the user to represent the various
        *types* of tiles present in the world, which are differentiated by different passability and/or traversal costs.

        The values of passable cells should then be used as keys for defining the costs that are passed to the `setCosts()` method.

        **Note:** Invoking this method resets the path cache.

        @param world An array with `width`x`height` elements containing the grid tile values.
    **/
    public inline function setWorld(world: Array<Int>)
    {
        if (world.length != width * height)
        {
            throw 'Invalid world size, expected: $width x $height = ${width*height}';
        }

        resetCache();

        this.world = world;
    }


    /**
        Sets the traversal costs for various cell values in the world.

        The `costs` map should have a key for each type of passable cell in the grid, which then specifies another map
        which contains the cost of movement **into** the respective cell in each direction.

        An example with 4-way movement, where only cells with value `4` are passable:

        ```haxe
        costs = [
            4 => [ N => 1, S => 1, W => 1, E => 1]
        ];
        ```

        Another example where cells with value `3` are *slopes*, where it's easier to move in from the south than the north.

        ```haxe
        costs = [
            3 => [ N => 1.5, S => 0.5, W => 1, E => 1]
        ];
        ```

        Only cells whose value is present in the `costs` map will be considered passable, all the rest will be treated as obstacles.

        **Note:** Invoking this method resets the path cache.

        @param costs The map of costs, as described above.
    **/
    public inline function setCosts(costs: Map<Int, Map<Direction, Float>>)
    {
        resetCache();

        this.costs = costs;
    }


    /**
        Overrides the default cost function, for calculating the heuristic distance
        that is part of the A* algorithm.

        The default function is chosen according to the movement directions.

        - `FourWay`: Manhattan Distance
        - `EightWay`: Diagonal Distamce

        For 8-way movement you may also consider the Euclidean distance.

        ```haxe
        setHeuristicFunction(new EuclideanDistance());
        ```

        **Note:** Invoking this method resets the path cache.
    **/
    public inline function setHeuristicFunction(heuristicFunction: HeuristicFunction)
    {
        resetCache();

        this.heuristicFunction = heuristicFunction;
    }


    /**
        Sets the weight with which the algorithm will consider the heuristic distance to the destination.

        The default weight is `1.0`

        This can be used as a tie-breaking method by setting it to a value slightly larger than `1.0`.

        ```
        weight = 1.0 + (minimum cost of taking one step)/(expected maximum path length)
        ```

        **Note:** Invoking this method resets the path cache.
    **/
    public inline function setHeuristicWeight(weight: Float)
    {
        resetCache();

        this.heuristicWeight = weight;
    }


    /**
        Tries to find the shortest path between the given points `(startX, startY)` and `(endX, endY)`,
        on the grid that was loaded using `setWorld()`.

        The returned object's `result` field contains a value specifying whether the search was successful,
        unsuccessful, or if there is no solution due to obstacles.
    **/
    public function solve(startX: Int, startY: Int, endX: Int, endY: Int): SearchResult
    {
        var result: SearchResult = new SearchResult(this);

        if (startX == endX && startY == endY)
        {
            result.result = StartEndSame;
            return result;
        }

        if (!inBounds(startX, startY) || !inBounds(endX, endY))
        {
            result.result = OutOfBounds;
            return result;
        }

        if (!isCellPassable(startX, startY) || !isCellPassable(endX, endY))
        {
            result.result = StartOrEndImpassable;
            return result;
        }

        var pathEndNode: PathNode = findPath(startX, startY, endX, endY, result);

        if (pathEndNode != null)
        {
            // Goal reached.
            result.setResultPath(pathEndNode);

            if (cacheEnabled)
            {
                pathCache.addSolution(pathEndNode);
            }

            return result;
        }

        // All nodes visited, couldn't reach the goal.
        result.result = NoSolution;
        return result;
    }


    /**
        Enables or disables path caching.

        Simply disabling the cache does not reset it.

        @param enabled Set to `true` to enable caching or `false` to disable it.
        @param size The new size of the cache, representing the amount of paths that should be cached
        at the same time. Passing this parameter will also reset and reconfigure the cache.
    **/
    public inline function configureCache(enabled: Bool, ?size: Int)
    {
        if (enabled)
        {
            cacheEnabled = true;

            if (pathCache == null)
            {
                if (size == null)
                {
                    throw "You need to specify a cache size when enabling path caching.";
                }

                pathCache = new PathCache(size);
            }
            else if (size != null)
            {
                // Size changed, reset and reconfigure the cache.
                resetCache();
                pathCache.configure(size);
            }
        }
        else
        {
            cacheEnabled = false;
        }
    }


    /**
        Resets the cache, emptying it of all cached paths.

        @param freeMemory By default, internal objects are pooled in order to minimize memory allocations and
        improve overall performance. Setting this parameter to `true` will clear all cache object pools in
        addition to resetting the cache, leaving all occupied memory out for the garbage collector.
    **/
    public inline function resetCache(freeMemory: Bool = false)
    {
        if (pathCache != null)
        {
            pathCache.reset(freeMemory);
        }
    }


    /**
        Returns the configured cost to move into a specific cell from a given direction.

        Will return `-1` if the cell has no costs specified (i.e is impassable).
    **/
    public inline function getCellCost(x: Int, y: Int, direction: Direction): Float
    {
        var cell: Int = getCellValue(x, y);
        if (costs.exists(cell) && costs[cell].exists(direction))
        {
            return costs[cell][direction];
        }
        else
        {
            return -1;
        }
    }


    /**
        Internal method for finding the shortest path between two points.

        @return The final node in the path, or `null` if a path could not be found.
    **/
    function findPath(startX: Int, startY: Int, endX: Int, endY: Int, result: SearchResult = null): PathNode
    {
        goalX = endX;
        goalY = endY;

        // Initialize lists.
        openList.clear();
        closedList.clear(nodePool);

        // Initialize starting node.
        var startingNode: PathNode = nodePool.getNode(startX, startY, 0, 0);

        // Put starting node in open list.
        openList.insert(startingNode);
        closedList.addNode(startingNode);

        while (!openList.isEmpty())
        {
            var node: PathNode = openList.popNext();
            node.visited = true;

            if (node.x == goalX && node.y == goalY)
            {
                // Goal reached.
                return node;
            }

            if (cacheEnabled && result != null)
            {
                // Can the cache complete the path from here to the end?
                var cacheResult: CacheResult = pathCache.solve(node.x, node.y, endX, endY, result);

                switch cacheResult
                {
                    case Hit:
                    {
                        // Hit, path found!
                        return node;
                    }


                    case NoSolution:
                    {
                        // The path is known to not have a solution.
                        return null;
                    }


                    case _:
                }
            }

            // Add the neighbors and move on to the next node.
            addNeighbors(node);
        }

        if(cacheEnabled)
        {
            pathCache.addNoSolution(startX, startY, endX, endY);
        }

        return null;
    }


    inline function addNeighbors(node: PathNode)
    {
        tryAddNeighbor(node, N);
        tryAddNeighbor(node, W);
        tryAddNeighbor(node, S);
        tryAddNeighbor(node, E);

        if (movementDirection.isDiagonal())
        {
            tryAddNeighbor(node, NW);
            tryAddNeighbor(node, NE);
            tryAddNeighbor(node, SW);
            tryAddNeighbor(node, SE);
        }
    }


    function tryAddNeighbor(node: PathNode, direction: Direction)
    {
        var neighborX: Int = node.x + direction.dirX();
        var neighborY: Int = node.y + direction.dirY();

        if (!inBounds(neighborX, neighborY))
        {
            // Out of bounds.
            return;
        }

        var neighborVal: Int = getCellValue(neighborX, neighborY);

        if (!canMoveFromCellInDirection(node.x, node.y, direction))
        {
            // Node not passable in the specified direction.
            // (i.e has no cost specified, or is blocked by a corner)
            return;
        }

        var costFromStart: Float = node.costFromStart + costs[neighborVal][direction];
        var costEstimateToGoal: Float = getHeuristicDistance(neighborX, neighborY, goalX, goalY);

        if (closedList.isDiscovered(neighborX, neighborY))
        {
            // Node has already been checked, check if previous path was better.
            var existing: PathNode = closedList.get(neighborX, neighborY);

            if (existing.visited)
            {
                // Node has already been visited, do nothing.
            }
            else if (existing.costFromStart <= costFromStart)
            {
                // Node has already been checked from a shorter path, do nothing.
            }
            else
            {
                // Current node is better than existing node, replace it.
                openList.remove(existing);

                existing.costFromStart = costFromStart;
                existing.estimatedCostToGoal = costEstimateToGoal;
                existing.previous = node;

                openList.insert(existing);
            }
            return;
        }

        var neighbor: PathNode = nodePool.getNode(neighborX, neighborY, costFromStart, costEstimateToGoal, node);

        neighbor.discovered = true;

        openList.insert(neighbor);
        closedList.addNode(neighbor);
    }


    inline function getCellValue(x: Int, y: Int): Int
    {
        return world[y * width + x];
    }


    inline function getHeuristicDistance(fromX: Int, fromY: Int, toX: Int, toY: Int): Float
    {
        if (heuristicFunction == null)
        {
            // Initialize a default heuristic function depending on the movement direction.
            heuristicFunction = switch movementDirection
            {
                case FourWay: new ManhattanDistance( getMinCost(N | W | E | S) );

                case EightWay | EightWayObstructed | EightWayHalfObstructed: new DiagonalDistance( getMinCost(N | W | E | S), getMinCost(NE | NW | SE | SW) );

                case _: throw 'Invalid movement direction.';
            }
        }

        return heuristicWeight * heuristicFunction.getEstimate(fromX, fromY, toX, toY);
    }


    inline function inBounds(x: Int, y: Int): Bool
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }


    inline function isCellPassable(x: Int, y: Int): Bool
    {
        return inBounds(x, y) && costs.exists(getCellValue(x, y));
    }


    inline function canMoveFromCellInDirection(x: Int, y: Int, direction: Direction): Bool
    {
        var neighborX: Int = x + direction.dirX();
        var neighborY: Int = y + direction.dirY();

        if (!isCellPassable(neighborX, neighborY))
        {
            // Neighbor in this direction has no costs specified.
            return false;
        }

        if (!costs[getCellValue(neighborX, neighborY)].exists(direction))
        {
            // Neighbor not passable from this direction.
            return false;
        }

        // Neighbor cell passable, check if diagonal movement is allowed.
        return switch movementDirection
        {
            // Given direction is not diagonal, no need to check.
            case _ if (!direction.isDiagonal()): true;

            // Normal EightWay can move despite being obstructed by adjacents.
            case EightWay: true;

            // One adjacent along the diagonal should be free in order to move diagonally.
            case EightWayHalfObstructed: isCellPassable(neighborX, y) || isCellPassable(x, neighborY);

            // Both adjacent along the diagonal should be free in order to move diagonally.
            case EightWayObstructed: isCellPassable(neighborX, y) && isCellPassable(x, neighborY);

            // Four-way movement, the first case should have handled this.
            default: throw 'Checking a diagonal direction in FourWay movement.';
        }
    }


    function getMinCost(directions: Int): Float
    {
        var found: Bool = false;
        var min: Float = 0;

        for (_ => dirCosts in costs)
        {
            for (dir => cost in dirCosts)
            {
                if (dir & directions > 0)
                {
                    if (!found || cost < min)
                    {
                        found = true;
                        min = cost;
                    }
                }
            }
        }

        return min;
    }


    /**
        The default set of 8-directional costs.

        ```haxe
        [
            Direction.N => 1.0,
            Direction.S => 1.0,
            Direction.E => 1.0,
            Direction.W => 1.0,

            Direction.NW => sqrt(2),
            Direction.SW => sqrt(2),
            Direction.NE => sqrt(2),
            Direction.SE => sqrt(2),
        ]
        ```
    **/
    public static var DefaultCosts(get, never): Map<Direction, Float>;
    @:noCompletion static final sqrt2: Float = sqrt(2);
    static inline function get_DefaultCosts(): Map<Direction, Float>
    {
        return [
            Direction.N => 1.0,
            Direction.S => 1.0,
            Direction.E => 1.0,
            Direction.W => 1.0,

            Direction.NW => sqrt2,
            Direction.SW => sqrt2,
            Direction.NE => sqrt2,
            Direction.SE => sqrt2,
        ];
    }
}
