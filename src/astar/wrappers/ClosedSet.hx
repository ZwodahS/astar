package astar.wrappers;

import astar.types.PathNodePool;
import astar.types.PathNode;


/**
    Wrapper over a 2D node array for marking cells that have been checked on the grid.
**/
abstract ClosedSet(Array<Array<PathNode>>)
{
    public inline function new(width: Int, height: Int)
    {
        this = new Array<Array<PathNode>>();
    }


    /**
        Adds a node to the closed set, marking its coordinates as checked.
    **/
    public inline function addNode(node: PathNode)
    {
        if (this.length <= node.x)
        {
            this.resize(node.x + 1);
        }

        if (this[node.x] == null)
            this[node.x] = new Array<PathNode>();

        if (this[node.x].length <= node.y)
        {
            this[node.x].resize(node.y + 1);
        }

        this[node.x][node.y] = node;
    }


    /**
        Checks if the coordinates of a node already exist in the set,
        indicating that it has already been checked.
    **/
    public inline function contains(x: Int, y: Int): Bool
    {
        return this.length > x
            && this[x] != null
            && this[x].length > y
            && this[x][y] != null;
    }


    /**
        Checks it a node has already been discovered at the given coordinates.
    **/
    public inline function isDiscovered(x: Int, y: Int): Bool
    {
        return contains(x, y) && this[x][y].discovered;
    }


    /**
        Checks it a node has already been visited at the given coordinates.
    **/
    public inline function isVisited(x: Int, y: Int): Bool
    {
        return contains(x, y) && this[x][y].visited;
    }


    /**
        Returns an existing node in the set at the given coordinates.
    **/
    public inline function get(x: Int, y: Int): PathNode
    {
        return this[x][y];
    }


    /**
        Clears the underlying 2D array, setting all elements to `null`.

        Any existing nodes that are not cached will be returned to the
        specified pool.
    **/
    public inline function clear(?pool: PathNodePool)
    {
        for (x in 0...this.length)
        {
            if (this[x] != null)
            {
                for (y in 0...this[x].length)
                {
                    remove(x, y, pool);
                }
            }
        }
    }


    /**
        Clears a specific position in the underlying 2D array.
        If a node exists in that position, and it is not cached,
        it will be returned to the specified pool.
    **/
    public inline function remove(x: Int, y: Int, ?pool: PathNodePool)
    {
        if (x < this.length && this[x] != null)
        {
            if (pool != null && this[x][y] != null)
            {
                pool.put(this[x][y]);
            }

            this[x][y] = null;
        }
    }
}
