package astar.cost;


interface HeuristicFunction
{
    function getEstimate(fromX: Int, fromY: Int, toX: Int, toY: Int): Float;
}
