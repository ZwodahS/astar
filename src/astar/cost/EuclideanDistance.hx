package astar.cost;

import Math.sqrt;


class EuclideanDistance implements HeuristicFunction
{
    var factor: Float;


    public inline function new(factor: Float = 1)
    {
        this.factor = factor;
    }


    public function getEstimate(fromX: Int, fromY: Int, toX: Int, toY: Int): Float
    {
        var dx: Int = toX - fromX;
        var dy: Int = toY - fromY;

        return factor * sqrt((dx * dx) + (dy * dy));
    }
}
