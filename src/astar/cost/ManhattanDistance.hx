package astar.cost;


class ManhattanDistance implements HeuristicFunction
{
    var factor: Float;


    /**
        @param factor The cost factor for lateral movement.
                      If not specified, the default value will be the minimum of all the specified costs in all directions.
                      For best results, this number should be equal to the normal cost of movement,
                      for one lateral step on normal terrain in the absence of obstacles.
                      If your world contains any kind of tile where movement is *faster* than on normal terrain,
                      for example ice tiles with cost `0.5` while the ground has cost `1`, then you should definitely
                      consider manually setting this value to the cost of movement on ground (`1`).
    **/
    public inline function new(factor: Float = 1)
    {
        this.factor = factor;
    }


    public function getEstimate(fromX: Int, fromY: Int, toX: Int, toY: Int): Float
    {
        var dx: Int = toX > fromX ? toX - fromX : fromX - toX;
        var dy: Int = toY > fromY ? toY - fromY : fromY - toY;

        return factor * (dx + dy);
    }
}
