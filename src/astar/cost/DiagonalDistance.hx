package astar.cost;

import Math.max;
import Math.min;


class DiagonalDistance implements HeuristicFunction
{
    var lateralFactor: Float;
    var diagonalFactor: Float;


    /**
        @param lateralFactor The cost factor for lateral movement.
                          If not specified, the default value will be the minimum of all the specified costs in all directions.
                          For best results, this number should be equal to the normal cost of movement,
                          for one lateral step on normal terrain in the absence of obstacles.
                          If your world contains any kind of tile where movement is *faster* than on normal terrain,
                          for example ice tiles with cost `0.5` while the ground has cost `1`, then you should definitely
                          consider manually setting this value to the cost of movement on ground (`1`).
        @param diagonalFactor Similar to `costFactor`, but used with the Diagonal distance heuristic.
                                  Only applies to variations of the `EightWay` movement direction.
                                  For reference, if `costFactor` is `1`, this should be approximately `sqrt(2)`.
    **/
    public inline function new(lateralFactor: Float = 1, diagonalFactor: Float = 1.41421356)
    {
        this.lateralFactor = lateralFactor;
        this.diagonalFactor = diagonalFactor;
    }


    public function getEstimate(fromX: Int, fromY: Int, toX: Int, toY: Int): Float
    {
        var dx: Int = toX > fromX ? toX - fromX : fromX - toX;
        var dy: Int = toY > fromY ? toY - fromY : fromY - toY;

        return lateralFactor * max(dx, dy) + (diagonalFactor - lateralFactor) * min(dx, dy);
    }
}
