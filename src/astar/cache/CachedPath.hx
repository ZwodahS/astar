package astar.cache;

import astar.types.PathNode;


@:publicFields
class CachedPath
{
    /** The x-coordinate of the starting node in the path. **/
    var startX: Int;

    /** The y-coordinate of the starting node in the path. **/
    var startY: Int;

    /** The x-coordinate of the final node in the path. **/
    var endX: Int;

    /** The y-coordinate of the final node in the path. **/
    var endY: Int;

    /** The end node. **/
    var startNode: CachedPathNode;

    /** Next linked object for pooling and bubble-sorting. **/
    var next: CachedPath;

    /** Previous linked object for bubble-sorting. **/
    var previous: CachedPath;

    /** Hash number identifying the start-end pair. **/
    var hash: Int;


    @:allow(astar.cache.CachedPathPool)
    private function new()
    {
        reset();
    }


    function reset()
    {
        startNode = null;
        hash = 0;
    }


    inline function matches(startX: Int, startY: Int, endX: Int, endY: Int): Bool
    {
        return this.startX == startX && this.startY == startY && this.endX == endX && this.endY == endY;
    }


    inline function hasSolution(): Bool
    {
        return startNode != null;
    }
}
