package astar.cache;

import astar.types.PathNode;
import astar.types.Pool;


class CachedPathNodePool extends Pool<CachedPathNode>
{
    public inline function new()
    {
        super();
    }


    public inline function getCachedPathNode(x: Int, y: Int, next: CachedPathNode, costToEnd: Float): CachedPathNode
    {
        var node: CachedPathNode = get();
        node.x = x;
        node.y = y;
        node.costToEnd = costToEnd;
        node.next = next;
        return node;
    }


    override inline function create(): CachedPathNode
    {
        return new CachedPathNode();
    }
}
