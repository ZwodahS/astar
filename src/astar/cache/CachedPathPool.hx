package astar.cache;

import astar.types.PathNode;
import astar.types.Pool;


class CachedPathPool extends Pool<CachedPath>
{
    public inline function new()
    {
        super();
    }


    public inline function getSolvedCachedPath(start: CachedPathNode, end: CachedPathNode, hash: Int): CachedPath
    {
        var path: CachedPath = get();
        path.startX = start.x;
        path.startY = start.y;
        path.endX = end.x;
        path.endY = end.y;
        path.startNode = start;
        path.hash = hash;
        return path;
    }


    public inline function getNoSolutionCachedPath(startX: Int, startY: Int, endX: Int, endY: Int, hash: Int): CachedPath
    {
        var path: CachedPath = get();
        path.startX = startX;
        path.startY = startY;
        path.endX = endX;
        path.endY = endY;
        path.startNode = null;
        path.hash = hash;
        return path;
    }


    override inline function create(): CachedPath
    {
        return new CachedPath();
    }
}
