package astar.cache;


@:publicFields
class CachedPathNode
{
    /** The x-coordinate of the node. **/
    var x: Int;

    /** The y-coordinate of the node. **/
    var y: Int;

    /** The exact cost from this node to the end of the path. **/
    var costToEnd: Float;

    /** Used both for pooling and reconstructing the path. **/
    var next: CachedPathNode;


    @:allow(astar.cache.CachedPathNodePool)
    private function new() { reset(); }


    inline function reset()
    {
        next = null;
    }
}
