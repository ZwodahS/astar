package astar;

import astar.types.Direction;
import astar.cache.CachedPathNode;
import astar.types.PathNode;
import astar.types.Result;
import astar.types.PathPoint;


@:allow(astar.Graph)
@:allow(astar.cache.PathCache)
class SearchResult
{
    /**
        Value indicating whether the search was successful.
        It is recommended to check this field before using `path`.
    **/
    public var result(default, null): Result = None;


    /**
        Amount of steps in the solution, i.e the number of nodes in the path.
    **/
    public var steps(default, null): Int = 0;


    /**
        The total exact (non-heuristic) cost of the path.
    **/
    public var cost(default, null): Float = 0;


    /**
        The solution's path.
        An ordered list of coordinates starting with the given `(startX, startY)`,
        and ending with the given `(endX, endY)`.

        Check the `result` before using.
    **/
    public var path(default, null): Array<PathPoint> = null;


    var graph: Graph;


    private function new(graph: Graph)
    {
        this.graph = graph;
    }


    /**
        Loads the found path by backtracking from the given end node.

        All nodes, iterated backwards, are each time prepended to the
        beginning of the path.

        @param node The final node in the path.
    **/
    private function setResultPath(node: PathNode)
    {
        result = Solved;

        if (path == null)
        {
            path = new Array<PathPoint>();
        }

        cost += node.costFromStart;

        if (path.length > 0 && path[0].x == node.x && path[0].y == node.y)
        {
            // Last node already in the path.
            // This occurs when a cached subpath was added first so this node overlaps.

            // Update the `costFromStart` of cached points.
            path[0].costFromStart = node.costFromStart;
            for (i in 1...path.length)
            {
                var stepDir: Direction = Direction.ofStep(path[i - 1].x, path[i - 1].y, path[i].x, path[i].y);
                var stepCost: Float = graph.getCellCost(path[i].x, path[i].y, stepDir);

                path[i].costFromStart = path[i - 1].costFromStart + stepCost;
            }

            // Skip the last node.
            node = node.previous;
        }

        while (node != null)
        {
            // Insert at the beginning since we are iterating backwards.
            path.insert(0, {
                x: node.x,
                y: node.y,
                costFromStart: node.costFromStart
            });

            steps++;

            node = node.previous;
        }
    }


    /**
        Appends a linked list of cached path nodes to the end of the search result's path..

        Meant to be called before `setResultPath()` when it is found that the remainder
        of the path exists in the cache.
    **/
    private function appendCachedNodes(node: CachedPathNode)
    {
        if (path == null)
        {
            path = new Array<PathPoint>();
        }

        cost += node.costToEnd;

        while (node != null)
        {
            path.push({
                x: node.x,
                y: node.y,
                costFromStart: -1
            });

            steps++;

            node = node.next;
        }
    }


    #if UNIT_TEST
    public function resetCost()
    {
        cost = 0;
        steps = 0;
    }
    #end
}
