package astar;


@:enum
abstract MovementDirection(Int) to Int
{
    /**
        Standard 4-way movement, where it is only
        possible to move to grid cells sharing an edge.

        Defaults to the **Manhatan distance** for the heuristic.
    **/
    var FourWay = 1 << 0;


    /**
        8-way movement, where it is possible to move diagonally
        for a total of eight directions.

        **Note:** With this type of movement, it possible for paths
        to go through corners that are normally obstructed by one or
        two adjacent tiles along the diagonal line.
        For alternatives `EightWayHalfObstructed` or `EightWayObstructed`
        should be considered.

        Defaults to the **Diagonal distance** for the heuristic.
    **/
    var EightWay = 1 << 1;


    /**
        8-way movement, where it is possible to move diagonally
        only when up to one of the adjacent tiles along the diagonal
        line to tha target is being obstructed.

        i.e when units can move diagonally over corners.

        Defaults to the **Diagonal distance** for the heuristic.
    **/
    var EightWayHalfObstructed = 1 << 2;


    /**
        8-way movement, where it is possible to move diagonally
        only when none of the adjacent tiles along the diagonal
        line to tha target area being obstructed.

        Defaults to the **Diagonal distance** for the heuristic.
    **/
    var EightWayObstructed = 1 << 3;


    public inline function isDiagonal(): Bool
    {
        return this & (EightWay | EightWayObstructed | EightWayHalfObstructed) > 0;
    }
}
