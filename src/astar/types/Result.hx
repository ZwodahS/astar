package astar.types;


@:enum
abstract Result(Int)
{
    /** Default value for no result. This should not be returned. **/
    var None = 0;

    /** The path was found. **/
    var Solved = 1;

    /** The search completed without finding a path. **/
    var NoSolution = 2;

    /** The goal point was the same as the given starting point. **/
    var StartEndSame = 3;

    /** One or both of the coordinates given were outside the bounds of the map. **/
    var OutOfBounds = 4;

    /** One or both of the coordinates given were impassable tiles. **/
    var StartOrEndImpassable = 5;
}
