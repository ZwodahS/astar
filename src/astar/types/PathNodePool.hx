package astar.types;


class PathNodePool extends Pool<PathNode>
{
    public inline function new()
    {
        super();
    }


    public inline function getNode(x: Int, y: Int, costFromStart: Float, costToGoal: Float, ?previous: PathNode): PathNode
    {
        var node: PathNode = get();
        node.x = x;
        node.y = y;
        node.costFromStart = costFromStart;
        node.estimatedCostToGoal = costToGoal;
        node.previous = previous;
        return node;
    }


    override inline function create(): PathNode
    {
        return new PathNode();
    }
}
