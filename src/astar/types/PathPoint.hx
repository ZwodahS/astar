package astar.types;


typedef PathPoint =
{
    /**
        The x-coordinate of a point in the map.
    **/
    var x: Int;

    /**
        The y-coordinate of a point in the map.
    **/
    var y: Int;

    /**
        The total cost from the beginning of the path to this point.
    **/
    var costFromStart: Float;
}
