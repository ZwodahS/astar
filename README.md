<div align="center">

[![ ](https://gitlab.com/haath/astar/-/raw/master/assets/logo.png)](https://gitlab.com/haath/astar)

[![pipeline status](https://gitlab.com/haath/astar/badges/master/pipeline.svg)](https://gitlab.com/haath/astar/pipelines/latest)
[![coverage report](https://gitlab.com/haath/astar/badges/master/coverage.svg)](https://gitlab.com/haath/astar/pipelines/latest)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/astar/blob/master/LICENSE)
[![release](https://img.shields.io/badge/release-haxelib-informational)](https://lib.haxe.org/p/astar/)

</div>

---

- Framework-agnostic A* solver for 2D maps.
- Efficient with minimal runtime memory allocations, pooling for all internal objects.
- Path caching, for significant speedup on consecutive searches with similar subpaths.
- Supporting both 4-way movement with the Manhatan distance heuristic, and 8-way movement with Diagonal distance.
- Support for defining custom heuristic functions.


## Installation

```bash
haxelib install astar
```


## Usage

```haxe
import astar.Graph;
```

#### First initialize a `Graph` object.

```haxe
// Dimensions of the world in tiles.
var worldWidth = 10;
var worldHeight = 10;

// Movement directions possible on the grid.
var movementDirection = FourWay;

var graph: Graph = new Graph(worldWidth, worldHeight, movementDirection);
```


#### Load the world grid into the graph.

```haxe
var world: Array<Int> = [
// x 0  1  2  3  4  5  6  7  8  9     y
    0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
    1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
    0, 0, 0, 2, 2, 0, 1, 1, 1, 1, // 4
    0, 0, 0, 2, 2, 0, 1, 0, 0, 0, // 5
    0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
    0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
    0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
    0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
];

graph.setWorld(world);
```

The array should have `width`x`height` elements, which contain the grid packed
row by row. So the first element is the cell 0x0, the second element is the cell 1x0 and so on.

The values in the cells of the grid are arbitrary, and shall be chosen by the user to represent the various
*types* of tiles present in the world, which are differentiated by different passability and/or traversal costs.

A cost is defined for traversal **into** a tile.


#### Set the costs of movement

```haxe
var costs = [
    // 0 is our ground, with a cost of 1 to move lateraly.
    // also: 0 => Graph.DefaultCosts,
    0 => [ N => 1, S => 1, W => 1, E => 1 ],

    // 2 is harsh terrain, slower to move into.
    2 => [ N => 1.5, S => 1.5, W => 1.5, E => 1.5 ]

    // 1 is wall tiles
    // without any cost specified, they are impassable
];

graph.setCosts(costs);
```


#### Solve for a path

```haxe
// Starting point
var startX = 3;
var startY = 3;

// End point
var endX = 7;
var endY = 9;

var result: SearchResult = graph.solve(startX, startY, endX, endY);

// Check if a solution was found
if (result.result == Solved)
{
    // Number of steps in the path
    var steps: Int = result.steps;

    // Total cost of the path
    var totalCost: Float = result.cost;

    // Go through the resulting path
    // (the first element is the starting point)
    for (point in result.path)
    {
        trace('${point.x}, ${point.y}');
    }
}
```


### Caching paths

Caching can be enabled, which can offer a significant speedup for multiple searches on the same paths or sub-paths.

```haxe
// Enable the cache, with a capacity of 512 paths.
graph.configureCache(true, 512);
```

The main method `graph.solve()` will then automatically query the cache to construct paths, as well as update it with paths that are solved.

The cache is always reset when updating the graph through the `setWorld()`, `setCosts()` and `setHeuristicFunction()`.
In addition to that it can also be reset manually.

```haxe
graph.resetCache();
```
